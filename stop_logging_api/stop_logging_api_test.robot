*** Settings ***
Library    RequestsLibrary
Library    Collections


*** variables ***
${base_url}=    http://localhost:5020/v1/eim



*** Test Cases ***
stop logging
    create session   mysession    ${base_url}
    ${body}=    create dictionary   deviceid=ab56
    ${header}=   create dictionary   Content-Type=application/json
    ${response}=    post request  mysession   /stop   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    200




stop logging
    create session   mysession    ${base_url}
    ${body}=    create dictionary   deviceid=ab56
    ${header}=   create dictionary   Content-Type=application/json
    ${response}=    post request  mysession   /stop?type=all   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    200




start logging method GET
    create session   mysession   ${base_url}
    ${body}=    create dictionary  deviceid=ab56
    ${header}=   create dictionary   Content-Type=application/json
    ${response}=    get request   mysession   /stop   data=${body}    headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"failure"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    400





start logging method PUT
    create session   mysession   ${base_url}
    ${body}=    create dictionary  deviceid=ab56
    ${header}=   create dictionary   Content-Type=application/json
    ${response}=    put request   mysession   /stop   data=${body}    headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"failure"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    400


start logging invalid json
    create session   mysession   ${base_url}
    ${body}=    create dictionary   a=b
    ${header}=   create dictionary   Content-Type=application/json
    ${response}=    post request   mysession   /stop   data=${body}    headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"failure"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    400




start logging valid device id
    create session   mysession   ${base_url}
    ${body}=    create dictionary   deviceid=ab561
    ${header}=   create dictionary   Content-Type=application/json
    ${response}=    post request   mysession   /stop   data=${body}    headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    200




start logging valid device id
    create session   mysession   ${base_url}
    ${body}=    create dictionary   device_id=ab561
    ${header}=   create dictionary   Content-Type=application/json
    ${response}=    post request   mysession   /stop   data=${body}    headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"failure"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    400




#start logging valid device id int
    #create session   mysession   ${base_url}
    #${body}=    create dictionary   deviceid=56
    #${header}=   create dictionary   Content-Type=application/json
    #${response}=    post request   mysession   /start   data=${body}    headers=${header}
    #log to console   ${response.status_code}
    #log to console   ${response.content}

    #VALIdATIONS
    #${response_body}=    convert to string  ${response.content}
    #should contain  ${response_body}    "status":"failure"
    #${status_code}=    convert to string    ${response.status_code}
    #should be equal    ${status_code}    400






