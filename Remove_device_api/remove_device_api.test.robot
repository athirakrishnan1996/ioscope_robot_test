*** Settings ***

Library     RequestsLibrary
Library     Collections




*** variables ***
${base_url}=    http://localhost:5020/v1/eim


*** Test Cases ***
delete_device
    create session  mysession   ${base_url}
    ${body}=    create dictionary   deviceid=ab56
    ${header}=  create dictionary   Content-Type=application/json
    ${response}=    post request    mysession   /remove   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    200



delete_device type all
    create session  mysession   ${base_url}
    ${body}=    create dictionary   deviceid=ab56
    ${header}=  create dictionary   Content-Type=application/json
    ${response}=    post request    mysession   /remove?type=all   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    200



delete_device get
    create session  mysession   ${base_url}
    ${body}=    create dictionary   deviceid=ab56
    ${header}=  create dictionary   Content-Type=application/json
    ${response}=    get request    mysession   /remove?type=all   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should not contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should not be equal   ${status_code}    200



delete_device put
    create session  mysession   ${base_url}
    ${body}=    create dictionary   deviceid=ab56
    ${header}=  create dictionary   Content-Type=application/json
    ${response}=    put request    mysession   /remove?type=all   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should not contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should not be equal   ${status_code}    200



delete_device invalid json
    create session  mysession   ${base_url}
    ${body}=    create dictionary   a=b
    ${header}=  create dictionary   Content-Type=application/json
    ${response}=    post request    mysession   /remove?type=all   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"failure"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal  ${status_code}    400



delete_device valid json
    create session  mysession   ${base_url}
    ${body}=    create dictionary   deviceid=ab561
    ${header}=  create dictionary   Content-Type=application/json
    ${response}=    post request    mysession   /remove?type=all   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal  ${status_code}    200



delete_device invalid json
    create session  mysession   ${base_url}
    ${body}=    create dictionary   device_id=ab561
    ${header}=  create dictionary   Content-Type=application/json
    ${response}=    post request    mysession   /remove?type=all   data=${body}   headers=${header}
    log to console   ${response.status_code}
    log to console   ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"failure"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal  ${status_code}    400



#delete_device deviceid int
    #create session  mysession   ${base_url}
    #${body}=    create dictionary   deviceid=56
    #${header}=  create dictionary   Content-Type=application/json
    #${response}=    post request    mysession   /remove?type=all   data=${body}   headers=${header}
    #log to console   ${response.status_code}
    #log to console   ${response.content}

    #VALIdATIONS
    #${response_body}=    convert to string  ${response.content}
    #should contain  ${response_body}    "status":"failure"
    #${status_code}=    convert to string    ${response.status_code}
    #should be equal  ${status_code}    400
