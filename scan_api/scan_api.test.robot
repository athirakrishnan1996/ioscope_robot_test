*** settings ***

Library    RequestsLibrary
Library    Collections


*** variables ***
${base_url}=    http://localhost:5020/v1/eim

${valid_body}
...     {
...         "a":"b"
...     }

${invalid_body}
...     {
...         \"  a    \"      : \"       b                 \"
...     }


*** Test Cases ***
scan_EIOCS
    create session    mysession    ${base_url}
    #${body}=    create dictionary    a=b
    #${header}=    create dictionary    Content-Type=application/json
    ${response}=    post request    mysession    /scan
    log to console  ${response.status_code}
    log to console  ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    200



scan_EIOCS all
    create session    mysession    ${base_url}
    ${response}=    post request    mysession    /scan?type=all
    log to console  ${response.status_code}
    log to console  ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    200



scan_EIOCS get
    create session    mysession    ${base_url}
    ${response}=    get request    mysession    /scan?type=all
    log to console  ${response.status_code}
    log to console  ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"failure"
    ${status_code}=    convert to string    ${response.status_code}
    should not be equal    ${status_code}    200



scan_EIOCS put
    create session    mysession    ${base_url}
    ${response}=    put request    mysession    /scan?type=all
    log to console  ${response.status_code}
    log to console  ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"failure"
    ${status_code}=    convert to string    ${response.status_code}
    should not be equal    ${status_code}    200




scan_EIOCS valid body
    create session    mysession    ${base_url}
    ${body}=    create dictionary    a=b
    ${header}=    create dictionary    Content-Type=application/json
    ${response}=    post request    mysession    /scan    data=${valid_body}    headers=${header}
    log to console  ${response.status_code}
    log to console  ${response.content}

    #VALIdATIONS
    ${response_body}=    convert to string  ${response.content}
    should contain  ${response_body}    "status":"success"
    ${status_code}=    convert to string    ${response.status_code}
    should be equal    ${status_code}    200



#scan_EIOCS invalid body
    #create session    mysession    ${base_url}
    #${header}=    create dictionary    Content-Type=application/json
    #${response}=    post request    mysession    /scan    data=${invalid_body}    headers=${header}
    #log to console    ${invalid_body}
    #log to console  ${response.status_code}
    #log to console  ${response.content}

    #VALIdATIONS
    #${response_body}=    convert to string  ${response.content}
    #should contain  ${response_body}    "status":"failure"
    #${status_code}=    convert to string    ${response.status_code}
    #should not be equal    ${status_code}    200





