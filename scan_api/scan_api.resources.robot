*** Variables ***

${http}     http://
${server}   localhost:8080
#${server}   test.ahssmart.com:8080
${userName}=   deepak.clt
${userId}=   542
${userId_temp}=   200
${userId_wrong} =   100001
${text}=    abcd
${mailId} =     "a@a.com"
${mailId_wrong} =   "a@a.com"
${phone}=      123456789
${phone_wrong}=      00001111
${address}=          "Address"
${firstName}=        "AAA"
${lastName}=         "bbbb"
${firstName_new}=        "Deepak1"
${lastName_new}=         "C1"
${gcmId}=   abcdewww1233
${deviceId}=    12344
${appId}=   HR
${appId_wrong}=   RR
${enableNotification}=  true

${property}             29681
${property2}             29612
${property_name}        Test Property
${tower}                29683
${tower_name}           Tower2
${floor}                29685
${LIV_id}               29690
${adminId}             1
${gateway_id}           29696
${gateway_id_wrong}     104
${sensor_id}           13669
${sensor_id_wrong}     104
${ssid}                Deepak
${ssid_wrong}          20000
${property_wrong}       1000000
${tower_wrong}          1000002
${tower_name_wrong}     Tower////111
${floor_wrong}          1000003
${LIV_wrong}            1000004
${serialKey}=           SGW1130002
${serialKey_wrong}=     11111
${resident_userId}=     414
${residentId}=          123456223
${medicationId}=        1
${medicationId_condt}=        3
${medicationId_wrong}=        100000005
${bulletinId}=  58863e1c684d141e34815394
${bulletinId_wrong}=  583e4a16684d1420e4583d9512123131131sasdadasdasdadsa
${ticketId}=   58624758684d142954ce0761
${ticketId_wrong}=    58624758684d142954ce0761oprgoahgoiahwighaithaw
${faqId}=   58624758684d142954ce0761
${faqId_wrong}=    58624758684d142954ce0761oprgoahgoiahwighaithaw

${new_user}
...     {
...         "firstName":"Deepak",
...         "lastName":"CLT",
...         "phone":"+919400468914",
...         "address":"ADDRESS",
...         "mailId":"deepak.clt@gmail.com",
...         "image": {},
...         "username":"deepak.clt"
...     }
${user_login_wrong}
...     {
...       "username": "deepak.clt@gmail.com",
...       "password": "eepuuqpm",
...       "appid": "RS"
...     }
${reset_password_wrong}
...     {
...          "username": "deepak.clt",
...          "oldPassword": "eepuuqpm",
...          "newPassword": "vuelogix"
...     }
${reset_password_wrong_username}
...     {
...          "username": "deepakc123",
...          "oldPassword": "eepuuqpm",
...          "newPassword": "vuelogix"
...     }
${Update_Liv}
...     {
...         "id": 29690,
...         "propertyName": "Test Property",
...         "tower": "Tower2",
...         "profile": "2 Bed",
...         "floor": "Floor2",
...         "unitNo": "LIV2",
...         "status": "To Do",
...         "gatewayId": 29696,
...         "propertyId": 29681,
...         "lockStatus":   {
...                         "by": {
...                         "image": {}
...                               }
...                         },
...         "occupancy":    {
...                         "state": "Vaccant"
...                         }
...     }
${Update_gateway}
...        {
...          "id": 29696,
...          "installStatus": "Not Installed",
...          "gatewayValidated": "Not Tested",
...          "bridgeValidated": "Not Tested",
...          "startTesting": "Stop Test",
...          "isGatewayReady": "false",
...          "livingunitId": "29690",
...          "serialKey": "null",
...          "ahsSensors": [
...	                        {
...	                            "status": "Not Installed",
...	                            "sensorType": "Water Leak",
...	                            "name": "Water Leak11",
...	                            "value": "Water leakage not detected",
...	                            "port": "2",
...	                            "testStatus": "Not Tested",
...	                            "inputType": "Enumeration",
...	                            "id": "13668"
...	                        }],
...	        "isGatewayReady": "true",
...	        "id": 29696
...        }
${Update_sensor}
...        {
...          "id": 13669,
...          "port": "3",
...          "name": "Main Door Status",
...          "value": "Door closed",
...          "status": "Not Installed",
...          "testStatus": "Not Tested",
...          "sensorType": "Main Door Status",
...         "inputType": "Enumeration"
...        }
${wifi_config}
...         {
...             "ssId": "Deepak",
...             "password": "deepak",
...             "securityType": "WEP"
...         }

${wifi_config_update}
...         {
...             "ssId": "Deepak",
...             "password": "00000000",
...             "securityType": "WEP"
...         }

${properties_update}
...        {
...          "propertyId": 29681,
...          "propType": "Property",
...          "images": [],
...          "location": {
...            "locationId": 529,
...            "name": "Test Property",
...            "address1": "Address1",
...            "address2": "Address2",
...            "city": "Calicut",
...            "state": "Kerala",
...            "zip": "673027",
...            "country": "India"
...          },
...          "numResidents": 1,
...          "numTotalUnits": 2,
...          "propertyManagers": [
...            {
...             "userId": 414,
...              "image": {}
...            }
...          ],
...          "installationEngineers": [],
...          "towerList": []
...        }
${Create_Resident}
...         {
...             "userDetails":
...             {
...                 "userId": 547
...             },
...         "livingUnitDetails":
...             {
...                 "id": 29690
...             }
...         }
${Create_Medication}
...        {
...          "name": "Dolo",
...          "type": "capsule",
...          "dosage": "2",
...          "description": "For fever"
...        }

${Update_Medication}
...        {
...          "medicationId": 3,
...          "name": "Herbal",
...          "type": "Syrup",
...          "dosage": "2 tea spoon",
...          "description": "For Cough"
...        }

${Resident_Medication}
...        {
...          "medicationDetails": {
...            "medicationId": 1
...          },
...         "quantity": "2",
...         "startDate": "2016-12-24T02:05:58Z",
...         "endDate": "2017-02-25T02:05:58Z"
...        }

${Create_Bulletin}
...        {
...          "propertyId": 29681,
...          "title": "Bulletin",
...          "description": "ASDF",
...          "creationTime": "2016-11-30T09:02:18Z",
...          "lastUpdate": "2016-11-30T09:02:18Z",
...          "state": "Active",
...          "scheduledStart": "2016-11-30T09:02:18Z",
...          "scheduledEnd": "2016-11-30T09:02:18Z",
...          "createdBy": {
...            "userId": 414,
...            "image": {}
...          }
...        }

${Update_Bulletin}
...        {
...          "id": "58863e1c684d141e34815394",
...          "propertyId": 29681,
...          "title": "Bulletin1234",
...          "lastUpdate": "2017-01-23T05:32:11Z",
...          "state": "Cancelled",
...          "scheduledStart": "2016-11-30T09:02:18",
...          "scheduledEnd": "2016-11-30T09:02:18Z",
...          "createdBy": {
...            "userId": 414,
...            "image": {}
...          }
...        }
${Create_Tickets}
...       {
...            "createdBy": {
...                "userId": 414
...            },
...            "assignedTo": {
...                "phone": "547"
...            },
...            "livingUnitDetails": {
...                "id": 29690,
...                "gatewayId": 29696,
...                "propertyId": 29681
...            },
...            "commonAreaDetails": {
...            "propertyName": "Test Property",
...            "propertyId": 29681
...            }
...        }
${Create_Faq}
...        {
...          "header": "What is a pendant1",
...          "body": "Pendat Details",
...          "tags": [],
...          "createdBy": {
...            "userId": 1,
...            "image": {}
...          },
...          "modifiedBy": {
...            "image": {}
...          }
...        }