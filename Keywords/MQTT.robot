*** Settings ***
Documentation    Suite description
Library                       MQTTLibrary
Library                       HttpLibrary.HTTP
Library                       BuiltIn
Test Timeout                  30 seconds


*** Variables ***
${broker.uri}          m13.cloudmqtt.com
${broker.port}          10901
${username}             aobhzodd
${password}             HOGafilEpsfy
${client.id}           robot-user1
${topic}                ahs/things/register/GWSK123897
${register topic}           ahs/things/register
${qos}                  0
${retention}            false
${message}              {	"time_stamp": "2016-08-10T12:32:56+05:30",	"client_token": "00008","state": {	"reported": { "register_type":"1"	}	},"metadata": {	"reported": {	"serial_key": "GWSK123897","living_unit":"1", "fw_version": "1.0"	}	}}
${API_ENDPOINT}         http://test.ahssmart.com:8080/ahs/rest/gateways
${gatewayId}            8
${update.gateway}           {"device": "8", "time_stamp": "2016-12-05T12:32:56+05:30","client_token": "00008", "state":{"reported": [{"sensor": "sensor.wired.sensor-3", "sensor_data": "0"}]}}
${update.sensor.topic}          ahs/things/8/update
${gateway.payload}          {"client_token": "00008","state": {"desired": {"device": "8","current_time":"Mon Dec 05 15:42:48 IST 2016"}}}
${sub.value}

*** Test Cases ***
Connect to MQTT Broker start
    set username and password           ${username}         ${password}
    connect         ${broker.uri}      ${broker.port}      ${client.id}      True
Publish to MQTT Synchronously
    publish          ${register topic}      ${message}       ${qos}

Subscribe
   ${result}=      subscribe       ${topic}            ${qos}       5
   log           ${result}


Get Gateway Details
   GET                                 ${API_ENDPOINT}/${gatewayId}
    ${result}=      get response body
	Response Status Code Should Equal     200

Update Sensor
    publish          ${update.sensor.topic}      ${update.gateway}       ${qos}

Get Updated Gateway Details
    GET                                 ${API_ENDPOINT}/${gatewayId}
    log response body
Disconnect from MQTT Broker
    disconnect
