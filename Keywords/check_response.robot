*** Settings ***
Documentation    Suite description
Library  HttpLibrary.HTTP
Library  Collections
Library  String
Library  json
Library     Dialogs
Library  ../RequestsKeywords.py
Library  OperatingSystem

*** Keywords ***
Must Get a 200 Success Message
    [Arguments]     ${resp}
    Log To Console  ${resp}
    Should Be Equal  ${resp.status_code}  ${200}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    [Return]    ${result}

Must Get a 202 Success Message
    [Arguments]     ${resp}
    Log To Console  ${resp}
    Should Be Equal  ${resp.status_code}  ${202}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    [Return]    ${result}
Must Get An 409 Message
    [Arguments]     ${resp}
    Log To Console  ${resp}
    Should Be Equal  ${resp.status_code}  ${409}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${statusCode}=             get json value  	${result}     	/statusCode
    Should Be Equal  ${statusCode}  409
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    log     ${statusMessage}
#    ${username}=   remove string      ${username}    "
#    Should Be Equal  ${statusMessage}       "Duplicate entry ${username} for key user_username"

Must Get An 401 Unauthorised Message
    [Arguments]     ${resp}
    Should Be Equal  ${resp.status_code}  ${401}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${statusCode}=             get json value  	${result}     	/statusCode
    Should Be Equal  ${statusCode}  401
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    log     ${statusMessage}
#    Should Be Equal  ${statusMessage}       "Invalid Username or Password."

Must Get An 401 Unauthorised inside Success Message
    [Arguments]     ${resp}
    Should Be Equal  ${resp.status_code}  ${200}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${statusCode}=             get json value  	${result}     	/statusCode
    Should Be Equal  ${statusCode}  401
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    log     ${statusMessage}

Must Get an 404 Message inside Succes message
    [Arguments]     ${resp}
    Should Be Equal  ${resp.status_code}  ${200}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${statusCode}=             get json value  	${result}     	/statusCode
    Should Be Equal  ${statusCode}  404
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    log     ${statusMessage}
#    Should Be Equal  ${statusMessage}       "No user found for User Id : ${userId_wrong}"
    [Return]    ${result}

Must Get an Success Message with 200 in Json
    [Arguments]     ${resp}
    Should Be Equal  ${resp.status_code}  ${200}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${statusCode}=             get json value  	${result}     	/statusCode
    Should Be Equal  ${statusCode}  200
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    log     ${statusMessage}
#    Should Be Equal  ${statusMessage}       "No user found for User Id : ${userId_wrong}"
    [Return]    ${result}

Must Get an 404 Message
    [Arguments]     ${resp}
    Should Be Equal  ${resp.status_code}  ${404}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${statusCode}=             get json value  	${result}     	/statusCode
    Should Be Equal  ${statusCode}  404
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    log     ${statusMessage}
#    Should Be Equal  ${statusMessage}       "No user found for User Id : ${userId_wrong}"
    [Return]    ${result}

Must Get an 405 Message
    [Arguments]     ${resp}
    Should Be Equal  ${resp.status_code}  ${405}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${statusCode}=             get json value  	${result}     	/statusCode
    Should Be Equal  ${statusCode}  405
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    log     ${statusMessage}
#    Should Be Equal  ${statusMessage}       "No user found for User Id : ${userId_wrong}"
    [Return]    ${result}

Must Get an 400 Message
    [Arguments]     ${resp}
    Should Be Equal  ${resp.status_code}  ${400}
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    log     ${result}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${statusCode}=             get json value  	${result}     	/statusCode
    Should Be Equal  ${statusCode}  400
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    log     ${statusMessage}
#    Should Be Equal  ${statusMessage}       "No user found for User Id : ${userId_wrong}"
    [Return]    ${result}


Must Get an Added True Message
    [Arguments]     ${resp}
    Should Be Equal As Strings  ${resp.status_code}  200
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${data}=             get json value  	${result}     	/data
    ${added}=             get json value  	${data}     	/added
    Should Be Equal  ${added}       true
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    Should Be Equal  ${statusMessage}       "OK"
    [Return]    ${result}

Must Get an Added false Message
    [Arguments]     ${resp}
    Should Be Equal As Strings  ${resp.status_code}  200
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${data}=             get json value  	${result}     	/data
    ${added}=             get json value  	${data}     	/added
    Should Be Equal  ${added}       false
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    Should Be Equal  ${statusMessage}       "OK"
    [Return]    ${result}

Must Get an Updated True Message
    [Arguments]     ${resp}
    Should Be Equal As Strings  ${resp.status_code}  200
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${data}=             get json value  	${result}     	/data
    ${updated}=             get json value  	${data}     	/updated
    Should Be Equal  ${updated}       true
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    Should Be Equal  ${statusMessage}       "OK"
    [Return]    ${result}

Must Get an Updated false Message
    [Arguments]     ${resp}
    Should Be Equal As Strings  ${resp.status_code}  200
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${data}=             get json value  	${result}     	/data
    ${updated}=             get json value  	${data}     	/updated
    Should Be Equal  ${updated}       false
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    Should Be Equal  ${statusMessage}       "OK"
    [Return]    ${result}

Must Get an Deleted True Message
    [Arguments]     ${resp}
    Should Be Equal As Strings  ${resp.status_code}  200
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
#    ${data}=             get json value  	${result}     	/data
    ${deleted}=             get json value  	${result}     	/deleted
    Should Be Equal  ${deleted}       true
#    ${statusMessage}=                get json value  	${result}     	/statusMessage
#    Should Be Equal  ${statusMessage}       "OK"
    [Return]    ${result}

Must Get an Deleted false Message
    [Arguments]     ${resp}
    Should Be Equal As Strings  ${resp.status_code}  200
    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${data}=             get json value  	${result}     	/data
    ${deleted}=             get json value  	${data}     	/deleted
    Should Be Equal  ${deleted}       false
    ${statusMessage}=                get json value  	${result}     	/statusMessage
    Should Be Equal  ${statusMessage}       "OK"
    [Return]    ${result}


Must Get an IOT 200 Message
    [Arguments]     ${resp}

    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${status}=             get json value  	${result}     	/status
    ${statusCode}=             get json value  	${status}     	/statusCode
    Should Be Equal As Strings  ${statusCode}  200
    ${statusMessage}=                get json value  	${status}     	/statusMessage
    Should Be Equal  ${statusMessage}       "OK"
    [Return]    ${result}

Must Get an IOT 404 Message
    [Arguments]     ${resp}

    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${status}=             get json value  	${result}     	/status
    ${statusCode}=             get json value  	${status}     	/statusCode
    Should Be Equal As Strings  ${statusCode}  404
    [Return]    ${result}

Must Get an IOT 401 Message
    [Arguments]     ${resp}

    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${status}=             get json value  	${result}     	/status
    ${statusCode}=             get json value  	${status}     	/statusCode
    Should Be Equal As Strings  ${statusCode}  401
    [Return]    ${result}

Must Get an IOT 400 Message
    [Arguments]     ${resp}

    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${status}=             get json value  	${result}     	/status
    ${statusCode}=             get json value  	${status}     	/statusCode
    Should Be Equal As Strings  ${statusCode}  400
    [Return]    ${result}

Must Get an IOT 500 Message
    [Arguments]     ${resp}

    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${status}=             get json value  	${result}     	/status
    ${statusCode}=             get json value  	${status}     	/statusCode
    Should Be Equal As Strings  ${statusCode}  500
    [Return]    ${result}

Must Get an IOT 405 Message
    [Arguments]     ${resp}

    ${json} =  Set Variable  ${resp.json()}
    ${result}=   stringify json      ${json}
    ${result}=   remove string using regexp      ${result}      (\[\'|\'\])
    log json    ${result}
    ${status}=             get json value  	${result}     	/status
    ${statusCode}=             get json value  	${status}     	/statusCode
    Should Be Equal As Strings  ${statusCode}  405
    [Return]    ${result}
