*** Settings ***
Documentation    Suite description
Library  HttpLibrary.HTTP
Library  Collections
Library  String
Library  json
Library     Dialogs
Library  ../RequestsKeywords.py
Library  OperatingSystem
*** Keywords ***
 POST Method with Json
    [Arguments]         ${serverAddress}    ${link}      ${jsonData}
    Create Session  httpbin  ${serverAddress}
    &{dict}=    evaluate    json.loads('''${jsonData}''')    json
    &{params}=   Create Dictionary      &{dict}
    ${headers}=   Create Dictionary   Content-type=application/json     Authorization=Bearer
    ${json_data}=   json.dumps   ${params}
    ${resp}=  Post Request   httpbin   ${link}   data=${json_data}   headers=&{headers}
    [Return]    ${resp}

POST Method with Json for List
    [Arguments]         ${serverAddress}    ${link}      @{listData}
    Create Session  httpbin  ${serverAddress}
    ${headers}=   Create Dictionary   Content-type=application/json     Authorization=Bearer
#    ${json_data}=   json.dumps   @{listData}
    ${resp}=  Post Request   httpbin   ${link}   data=@{listData}   headers=&{headers}
    [Return]    ${resp}

POST Method with URL
    [Arguments]         ${serverAddress}    ${link}      ${params}
    Create Session  httpbin  ${serverAddress}${link}
    ${resp}=    Post Request   httpbin      /post       params=&{params}
    Log To Console  ${resp}
    [Return]    ${resp}

GET Method
    [Arguments]         ${serverAddress}    ${link}
    Create Session  httpbin  ${serverAddress}${link}
    ${resp}=    get request  httpbin      /
    [Return]    ${resp}

GET Method with Parameters
    [Arguments]         ${serverAddress}    ${link}     ${params}
    Create Session  httpbin  ${serverAddress}${link}
    ${resp}=    get request  httpbin     /get       params=&{params}
    [Return]    ${resp}

GET Method with Parameters without URI
    [Arguments]         ${serverAddress}    ${link}     ${params}
    Create Session  httpbin  ${serverAddress}${link}
    ${resp}=    get request  httpbin     /       params=&{params}
    [Return]    ${resp}

PUT Method with Json
    [Arguments]         ${serverAddress}    ${link}      ${jsonData}
    Create Session  httpbin  ${serverAddress}
    &{dict}=    evaluate    json.loads('''${jsonData}''')    json
    &{params}=   Create Dictionary      &{dict}
    ${headers}=   Create Dictionary   Content-type=application/json     Authorization=Bearer
    ${json_data}=   json.dumps   ${params}
    ${resp}=  Put Request   httpbin   ${link}   data=${json_data}   headers=&{headers}
   [Return]    ${resp}

PUT Method with URL Parameters
    [Arguments]         ${serverAddress}    ${link}      ${params}
    Create Session  httpbin  ${serverAddress}${link}
    ${resp}=   Put Request   httpbin      /put       params=${params}
   [Return]    ${resp}

Delete Method
    [Arguments]         ${serverAddress}    ${link}      ${params}
    Create Session  httpbin  ${serverAddress}${link}
    ${resp}=  DELETE REQUEST  httpbin  /DELETE      params=&{params}
    [Return]    ${resp}

Delete Method Without Parameters
     [Arguments]         ${serverAddress}    ${link}
    Create Session  httpbin  ${serverAddress}${link}
    ${resp}=  DELETE REQUEST  httpbin  /
    [Return]    ${resp}
