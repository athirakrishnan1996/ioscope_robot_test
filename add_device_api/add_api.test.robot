*** Settings ***
Library    RequestsLibrary
Library    Collections
Library    OperatingSystem
Library    JSONLibrary


*** variables ***

${base_url}=    http://localhost:5020/v1/eim



*** Test Cases ***

Add_device
    create session  mysession   ${base_url}
    ${json_obj}=    load json from file    /home/user/Documents/json_files/validbody.json
    ${body}=    create dictionary   ${json_obj}
    ${header}=  create dictionary   Content-Type=application/json
    ${response}=    post request    mysession    /add   data=${body}    headers=${header}
    log to console  ${response.status_code}
    log to console  ${response.content}
    log to console  ${response.header}

    #VALIdATIONS
    #${response_body}=   convert to string   ${response.content}
    #should contain  ${response_body}    "status":"success"
    #${status_code}=    convert to string    ${response.status_code}
    #should be equal   ${status_code}    200




