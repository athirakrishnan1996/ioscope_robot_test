*** settings ***

library     RequestsLibrary
Library     Collections


*** variables ***

${base_url}   http://localhost:5020/v1/eim


*** test cases ***

Get EIOC status
    create session   mysession   ${base_url}
    ${response}=  Get request   mysession   /status
    #Log  ${response.status_code}
    #Log  ${response.content}
    #Log  ${response.headers}

    #validations

    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}   200

    ${body}=   convert to string   ${response.content}
    should contain   ${body}   "idx": 64


Get EIOC status all
    create session   mysession   ${base_url}
    ${response}=   Get request   mysession   /status?type=all

    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}   200

    ${body}=   convert to string   ${response.content}
    should contain   ${body}   "idx": 64

Get EIOC status logging
    create session   mysession   ${base_url}
    ${response}=   Get request   mysession   /status?type=logging

    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}   200

    ${body}=   convert to string   ${response.content}
    should contain   ${body}   "idx": 64

Get EIOC status logging
    create session   mysession   ${base_url}
    ${response}=   Get request   mysession   /status?type=logging

    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}   200

    ${body}=   convert to string   ${response.content}
    should not contain   ${body}   "idx": 68


Get EIOC status scan
    create session   mysession   ${base_url}
    ${response}=   Get request   mysession   /status?type=scan

    ${status_code}=   convert to string   ${response.status_code}
    should be equal   ${status_code}   200

    ${body}=   convert to string   ${response.content}
    should contain   ${body}   "idx": 82



